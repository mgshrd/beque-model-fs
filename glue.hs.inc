type BequeResult ok err = Prelude.Either err ok

coqstring2string :: String -> Prelude.String
coqstring2string s = case s of { EmptyString -> ""; String0 c s' -> c:(coqstring2string s') }
string2coqstring :: Prelude.String -> String
string2coqstring s = Prelude.foldr (\e accu -> String0 e accu) EmptyString s

exception2errorcode :: Control.Exception.SomeException -> String
exception2errorcode = string2coqstring Prelude.. Prelude.show

trymap :: Prelude.IO a -> Prelude.IO (Prelude.Either String a)
trymap = (Prelude.fmap (Data.Bifunctor.first exception2errorcode)) Prelude.. logerr Prelude.. Control.Exception.try
  where logerr a = do
                     v <- a
                     case v of {
                       Prelude.Left e -> let es = Prelude.show e in
                                         System.Log.Logger.errorM "" (Prelude.show e);
                       Prelude.Right _ -> Prelude.return () }
                     Prelude.return v
