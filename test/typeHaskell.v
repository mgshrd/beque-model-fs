Require Ascii.
Require String.
Require Import Omega.

Require Import ltac_utils.
Require Import sublist_facts.
Require Import result.
Require Import ltac_seg.
Require Import case_tag.

Require Import World.
Require Import IORaw.
Require Import IODebug.
Require Import IONoDebug.
Require Import ResultUtils.

Require Import ElemHandle.

Require Import FD.
Require Import FS.
Require Import FSActions.

Module TypeM
       (w : World)
       (ior : IORaw w)

       (id : ElemHandle)
       (fd : FD w ior id)

       (daa : FSActions w ior id fd)
       (fs : FS w ior id fd daa)
.

Module fsx := fs.

Module iod := IODebug w ior.
(*Module iod := IONoDebug w ior.*)
Import iod.

Module ru := ResultUtils w ior.
Import ru.

Ltac overlappingsegs H0 H :=
  match type of H with
  | fs.segment_of_action_with_res _ _ (?x ++ ?a) =>
    match type of H0 with
    | fs.segment_of_action_with_res _ _ (?b ++ ?x) =>
      match goal with
      | H1 : b <> nil |- _ =>
        let Hx := fresh in
        elim (fun pf pf2 => fs.segment_no_recombine H0 x pf pf2 H);
        [ exists b; split; [ assumption | ]; split; [ | reflexivity ]; intro Hx; destruct b; [ elim H1; reflexivity | ]; ltac_sublist.lengthelim Hx; omega
        | exists a; reflexivity
        ]
      | H1 : a <> nil |- _ =>
        let Hx := fresh in
        elim (fun pf pf2 => fs.segment_no_recombine H0 x pf pf2 H);
        [ exists b; split; [ assumption | ]; split; [ | reflexivity ]
        | exists a; reflexivity
        ]
      end
    end
  end.

Ltac subsegof H H0 :=
  let Hx := fresh in
  assert (Hx := fun pf pf2 => fs.segment_entirety H0 _ pf pf2 H);
  elim Hx; clear Hx;
  [ idtac
  | intro Hx;
    match type of Hx with
    | ?x = (?x ++ ?y)%list =>
      match goal with
      | H : y <> nil |- _ => destruct y; [ elim H; reflexivity | ]; ltac_sublist.lengthelim Hx; omega
      end
    | ?x = (?y ++ ?x)%list =>
      match goal with
      | H : y <> nil |- _ => destruct y; [ elim H; reflexivity | ]; ltac_sublist.lengthelim Hx; omega
      end
    | ?x = (?y ++ ?x ++ _)%list =>
      match goal with
      | H : y <> nil |- _ => destruct y; [ elim H; reflexivity | ]; ltac_sublist.lengthelim Hx; omega
      end
    | ?x = (_ ++ ?x ++ ?y)%list =>
      match goal with
      | H : y <> nil |- _ => destruct y; [ elim H; reflexivity | ]; ltac_sublist.lengthelim Hx; omega
      end
    | ?x = (?x ++ _::_)%list =>
      ltac_sublist.lengthelim Hx; omega
    | ?x = ((_::_) ++ ?x)%list =>
      ltac_sublist.lengthelim Hx; omega
    | ?x = ((_::_) ++ ?x ++ _)%list =>
      ltac_sublist.lengthelim Hx; omega
    end
  ].

Lemma tillstop :
  forall starts keeps s t,
    fs.ltl.tillnow starts keeps (s ++ t) ->
    ~keeps (s ++ t)%list ->
    False.
Proof.
  intros.
  assert (Hx := fs.ltluf.tillnow_unfold_seg _ _ _ H).
  sega. ltac_sublist.pfelim tmp.
  - subst s. subst x0. clear tmp.
    destruct H. destruct H. destruct H. destruct H3. rewrite <- List.app_assoc in H.
    ltac_sublist.pfelim H.
    + subst x. subst x2. clear H.
      elim H0. repeat rewrite <- List.app_assoc. apply H4.
      exists nil. reflexivity.
    + subst x0. subst x2. elim H0. rewrite <- List.app_assoc. apply H4. exists nil. reflexivity.
    + subst x0. clear H. ltac_sublist.pfelim H7.
      * subst x1. subst x2. clear H7.
        elim H0. rewrite List.app_assoc. rewrite <- List.app_assoc. apply H4. exists nil. reflexivity.
      * subst x3. subst x2. clear H7.
        elim H0. apply H4. exists nil; reflexivity.
      * subst x3. subst t. clear H7.
        elim H0. rewrite List.app_assoc. rewrite <- (List.app_assoc _ _ x0). apply H4. exists nil; reflexivity.

  - subst x. subst x0. clear tmp.
    elim H0. apply H1. exists nil. reflexivity.

  - subst x. subst t. clear tmp.
    destruct H. destruct H. destruct H. destruct H3.
    ltac_sublist.pfelim H.
    + subst s. subst x2.
      elim H0. rewrite <- List.app_assoc. apply H4. exists nil; reflexivity.
    + subst x. subst x2. clear H.
      elim H0. apply H4. exists nil; reflexivity.
    + subst x. ltac_sublist.pfelim H7.
      * subst x1 x2. clear H7 H.
        elim H0. rewrite <- List.app_assoc. rewrite List.app_assoc. apply H4. exists nil; reflexivity.
      * subst x3 x2. clear H7 H.
        elim H0. rewrite List.app_assoc. apply H4. exists nil. reflexivity.
      * subst x3 x0. clear H7 H.
        elim H0. rewrite List.app_assoc. apply H1. exists nil; reflexivity.
Qed.

(*Local Obligation Tactic := try tauto.
Program Fixpoint dump
        (r w : id.handle)
        (max_stream : nat) :
  IO_cmd
    (fun t => fs.isvalidref r t /\ fs.isvalidref w t)
    (fun s t =>
       forall ss,
         sublist.sublist_of ss s ->
         (forall a res, ~fs.segment_of_action_with_res a res ss) \/
         (exists n buf, fs.segment_of_action_with_res (daa.dw_action_operate (fd.da.fd_read r n)) buf ss) \/
         (exists buf n, fs.segment_of_action_with_res (daa.dw_action_operate (fd.da.fd_write w buf)) n ss)) :=
  match max_stream with
  | O => |\? "eos strengthen" \ /|? "eos weaken" \ IO_return tt
  | S ms' =>
    do? "dump it fin" \
      buf <?--? "read pre" \ fs.read r 10;;
      wc <?--? "write pre" \ fs.write w buf;;
      /|? "rec dump" \ dump r w ms'
  end.
Obligation 2.
{
  intros.
  left. intros. intro. simpl in H0. destruct H0. subst. destruct H1. destruct H0. destruct x; [ destruct ss; [ destruct x0 | ] | ]; try discriminate H0.
  assert (Hx := fs.segment_not_nil H2). exact Hx.
}
Obligation 4.
{
  intros. destruct H as [ _ H ].
  unfold fs.isvalidref in *. sega. destruct H1. exists x1.
  assert (Hx := fun pf => fs.propagates_correct pf H0 H1).
  apply Hx. simpl. exact I.
}
Obligation 5.
{
  intros.
  split; [ constructor | ].
  intros. sega.
  unfold fs.isvalidref in *. destruct H, H2.
  split; [ exists x0 | exists x3 ].
  - assert (Hx := fun pf => fs.propagates_correct pf H1 H).
    assert (Hy := fun pf => fs.propagates_correct pf H0 (Hx I)).
    apply Hy. exact I.
  - assert (Hx := fun pf => fs.propagates_correct pf H1 H2).
    assert (Hy := fun pf => fs.propagates_correct pf H0 (Hx I)).
    apply Hy. exact I.
}
Obligation 7.
{
  cbv beta. intros. sega. destruct H as [ _ [ rvalid wvalid ] ]. unfold projT1 in *. mcase x.
  - desig. sega. simpl in *. mcase x.
    + destruct H1. destruct H1.
      ltac_sublist.pfelim H1.
      * subst x2. ltac_sublist.pfelim H7.
        {
          subst x4 x0. clear H7 H1.
          apply H4. exists x. exists x2. reflexivity.
        }
        {
          subst x0 x4. clear H7 H1. apply H4. exists x. exists nil. rewrite List.app_nil_r. reflexivity.
        }
        {
          subst ss. ltac_sublist.pfelim H9.
          - subst x3 x0. clear H9 H7 H1.
            left. intros. intro. clear H4.
            overlappingsegs H1 H.
          - subst x2 x0. clear H9 H7.
            left. intros. intro. clear H4.
            subsegof H H7.
            exists x4. exists nil. rewrite List.app_nil_r. reflexivity.
          - subst x2 x1. clear H9 H7. clear H4.
            left. intros. intro.
            subsegof H H4.
            exists x4. exists x5. reflexivity.
        }
      * subst x2. ltac_sublist.pfelim H6.
        {
          subst x3 x0. clear H6 H1. clear H4.
          left. intros. intro.
          subsegof H1 H.
          exists nil. exists x2. reflexivity.
        }
        {
          subst x1 x3. clear H1. clear H4.
          right. right. exists s. exists (ok n). exact H.
        }
        {
          subst x1 ss. clear H4. clear H6 H1.
          left. intros; intro.
          subsegof H H1.
          exists nil. exists x2. reflexivity.
        }
      * subst x. ltac_sublist.pfelim H7.
        {
          subst x3. ltac_sublist.pfelim H9.
          - subst x x0. clear H4. clear H9 H7 H1.
            left. intros; intro.
            subsegof H1 H.
            exists x4. exists x3. reflexivity.
          - subst x x1. clear H9 H7 H1. clear H4.
            left; intros; intro.
            subsegof H1 H.
            exists x4. exists nil. rewrite List.app_nil_r. reflexivity.
          - subst ss x1. clear H9 H7. clear H4.
            left; intros; intro.
            overlappingsegs H H4.
        }
        {
          subst x4 x1. clear H1 H7. clear H4 x2. clear H H5. clear H3.
          destruct x0.
          - right; left. exists 10. exists (ok s). rewrite List.app_nil_r in H2. exact H2.
          - left. intros. intro.
            subsegof H H2.
            exists nil. exists (m::x0)%list. reflexivity.
        }
        {
          subst x4 x1. clear H1 H7. clear H4.
          left. intros; intro.
          subsegof H1 H2.
          exists x. exists x0. reflexivity.
        }
    + subst x2. destruct H1. destruct H1. simpl in H1. ltac_sublist.pfelim H1.
      * subst x3. ltac_sublist.pfelim H6.
        {
          subst x2 x0. clear H6 H1.
          left. intros. intro.
          subsegof H1 H.
          exists x. exists x3. reflexivity.
        }
        {
          subst x2 x1. clear H1 H6.
          destruct x.
          - right; right. exists s. exists (err n). exact H.
          - left. intros. intro.
            subsegof H1 H.
            exists (m::x)%list. exists nil. rewrite List.app_nil_r. reflexivity.
        }
        {
          subst ss x1. clear H6 H1.
          left. intros; intro.
          overlappingsegs H1 H2.
        }
      * subst x3 x1. clear H1.
        destruct x0.
        {
          right. left. exists 10. exists (ok s). rewrite List.app_nil_r in H2. exact H2.
        }
        {
          left. intros; intro.
          subsegof H1 H2.
          exists nil. exists (m::x0)%list. reflexivity.
        }
      * subst x x1. clear H1 H3.
        left. intros; intro.
        subsegof H1 H2.
        exists x2. exists x0; reflexivity.

  - subst x0. destruct H1. destruct H. simpl in H. subst x1.
    destruct x.
    + destruct x0.
      * right. left. exists 10. exists (err n). rewrite List.app_nil_r in H2. exact H2.
      * left. intros; intro.
        unfold app in H2 at 1.
        subsegof H H2.
        exists nil. exists (m::x0)%list. reflexivity.
    + left. intros; intro.
      subsegof H H2.
      exists (m::x)%list. exists x0. reflexivity.
}*)

Definition type
           (path : daa.name) :
  IO_cmd (fun t => forall h, ~fs.isopenfor h path t) (fun s t => forall h n, fs.isopenfor h n t <-> fs.isopenfor h n (s ++ t)).
Proof.

  Open Scope string_scope.
  unshelve refine (do? "fin" \
            r <?--? "open pre" \ fs.open path;;
            _ <--? "write pre" \ fs.write r "你好，世界 Hello World!
";;
            /|? "close pre" \ fs.close r); try solve [ auto; tauto ].
  Close Scope string_scope.

  {
    intros.
    unfold fd.action_for.
    destruct H as [ _ H ]. sega.
    unfold fs.isvalidref. eexists.
    exact H1.
  }

  {
    intros. split; [ constructor | ]. intros. sega.
    unfold fs.isvalidref. eexists.
    apply (fun pp => fs.propagates_correct pp H0 H2).
    simpl. exact I.
  }

  {
    intros ? ? ? ? ?.
    sega. rename x1 into openseg. unfold projT1 in *. destruct H as [ _ H ].
    rename x into openres; mcase openres; [ rename openseg into opensuccseg | rename openseg into openfailseg ]; simpl in *.
    - sega.
      rename x2 into writeseg. rename x1 into closeseg.
      intros; split; intro.
      + assert (Hx := fun ppoo => fs.propagates_correct ppoo H1 H6).
        assert (Hy := fun ppwo => fs.propagates_correct ppwo H4 (Hx I)).
        assert (Hz := fun ppco => fs.propagates_correct ppco H5 (Hy I)).
        apply Hz; clear Hx Hy Hz.
        simpl. apply H3. unfold fs.isvalidref. eexists. exact H6.
      + assert (Hx := fun ppco => fs.back_propagates_correct ppco H5 H6).
        assert (Hy := fun ppwo => fs.back_propagates_correct ppwo H4 (Hx I)).
        assert (Hz := fun ppoo => fs.back_propagates_correct ppoo H1 (Hy I)).
        apply Hz; clear Hx Hy Hz.
        simpl. intro Hx; inversion Hx; clear Hx; subst h0.
        unfold fs.isvalidref, fs.isopenfor in *.
        apply (tillstop _ _ _ _ H6). intro.
        destruct H7.
        * assert (Hx := fun pf => H7 _ _ _ pf H5). elim Hx.
          eexists; reflexivity.
        * destruct H7 as [ ? [ ? [ ? [ ? [ ? ? ] ] ] ] ].
          destruct H7. ltac_sublist.pfelim H7.
          {
            subst closeseg x3. clear H7.
            subsegof H8 H5. eexists nil. eexists; reflexivity.
          }
          {
            subst x2 x3. clear H7.
            fs.sameseg H5 H8. simpl in *. elim H9. reflexivity.
          }
          {
            subst x2.
            subsegof H5 H8. exists nil. eexists; reflexivity.
          }

    - subst x0. simpl. cleartriv.
      intros. split; intro.
      + apply (fun pf => fs.propagates_correct pf H1).
        * simpl. exact I.
        * exact H2.
      + apply (fun pf => fs.back_propagates_correct pf H1).
        * simpl. intro d; discriminate d.
        * exact H2.
  }
Defined.

End TypeM.

Require Import World.
Require Import AxiomaticIO.
Require DelayExtractionDefinitionToken.
Module HWW
<: World
.

Axiom pty : Set.

Module extractHaskell (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant pty => "() -- never use".
End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant pty => "() (*never use*)".
End extractOcaml.

Definition msg := pty.

End HWW.

Module HWWExtract.
Include HWW.extractHaskell DelayExtractionDefinitionToken.token.
End HWWExtract.

Module ior.
Include AxiomaticIO HWW.
End ior.
Module iorExtract.
Include ior.extractHaskell DelayExtractionDefinitionToken.token.
End iorExtract.

Module FDH
<: ElemHandle
.

Axiom handle : Set.
Axiom eq_handle_dec :
  forall h1 h2 : handle,
    {h1=h2}+{h1<>h2}.

Module extractHaskell (x : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant handle => "System.IO.Handle".
Extract Constant eq_handle_dec => "(Prelude.==)".
End extractHaskell.

Module extractOcaml (x : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant handle => "Unix.file_descr".
Extract Constant eq_handle_dec => "(=)".
End extractOcaml.

End FDH.
Module FDHExtract.
Include FDH.extractHaskell DelayExtractionDefinitionToken.token.
End FDHExtract.

Require Import FDActions.
Require Import FDImpl.
Require DelayExtractionDefinitionToken.
Require Import FSImpl.

Module fd.
Include FDImpl HWW ior FDH.
End fd.
Module fdExtract := fd.extractHaskell DelayExtractionDefinitionToken.token.

Require Import FSActionsImpl.
Module fsa := FSActionsImpl HWW ior FDH fd.
Module fs.
Include FSImpl
               HWW ior
               FDH fd
               fsa.
End fs.
Module fsExtract.
Include fs.extractHaskell DelayExtractionDefinitionToken.token.
End fsExtract.

Module m := TypeM HWW ior FDH fd fsa fs.

Extraction Language Haskell.

Require Import common_extract.
Require DelayExtractionDefinitionToken.

Module commonExtract.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End commonExtract.

Require String.

Module Argv.

Axiom getArgs : ior.IO (fun _ => True) (list String.string) (fun _ s _ => s = nil).

Module extractHaskell (token: DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant getArgs => "Prelude.fmap (Prelude.fmap string2coqstring) System.Environment.getArgs".
End extractHaskell.

Module extractOcaml (token: DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant getArgs => "let skiparg0 = ref true in let r = ref [] in Array.iter (fun a -> if !skiparg0 then skiparg0 := false else r := (string2coqstring a)::!r) Sys.argv; (fun () -> !r)".
End extractOcaml.

End Argv.

Module argvExtract.
Include Argv.extractHaskell DelayExtractionDefinitionToken.token.
End argvExtract.

Open Scope string_scope.
Import m.iod.
Program Definition main : IO_cmd (fun t => t = nil) (fun _ _ => True).
  refine (do
            args <-- Argv.getArgs;;
            /| m.type match args with nil => "/dev/tty" | cons h _ => h end); try solve [ auto; tauto ].
Close Scope string_scope.
Proof.
  intros. intro.
  sega.
  unfold fs.isopenfor in H0. unfold fs.prop_to_trace_prop in H0. unfold fs.simple_propagation in H0.
  unfold fs.ltl.tillnow in H0. sega. clear H.
  destruct x; [ destruct x0 | ]; try discriminate tmp.
  destruct H0. destruct H. destruct H.
  destruct x; [ destruct x0 | ]; try discriminate H.
  apply (m.fsx.segment_not_nil H0).
Defined.

Set Warnings "-extraction-logical-axiom".
Extraction "typeHaskell" main.

