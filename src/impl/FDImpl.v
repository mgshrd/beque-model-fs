Require Import String.
Require Import PeanoNat.
Import Nat.
Require Import Omega.

Require Import sublist.
Require Import ltac_utils.
Require Import cheap_app_assoc.
Require Import result.
Require Import dec_utils.

Require Import Beque.IO.intf.World.
Require Import Beque.IO.intf.IORaw.
Require Import Beque.IO.intf.IOAux.

Require Import Beque.LTL.util.LTL.

Require Import Beque.Frame.intf.array.ElemDomain.
Require Import Beque.Frame.intf.array.ElemHandle.
Require Import Beque.Frame.util.ExistentialAction.

Require Import Beque.Util.DelayExtractionDefinitionToken.

Require Import FDActions.
Require Import FD.

Module FDImpl
       (w : World)
       (ior : IORaw w)
       (id : ElemHandle)
<: FD w ior id
.

Inductive fd_prop :=.
(************************** first part of [Domain] ***************************)
Definition prop := fd_prop.

Module da := FDActions id.

Export da.

(************************** [ElemDomain] part ***************************)

Definition prop_for :
  prop -> id.handle.
Proof.
  intro. destruct H.
Defined.

Definition action_for
           a :=
  match a with
    | da.fd_read h _ => h
    | da.fd_write h _ => h
    | da.fd_sync h => h
  end.

(************************** rest of [Domain] ***************************)
Module io := IOAux w ior.
Module ltl := LTL w.


(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
Definition action := da.action.
Definition eq_action_dec := da.eq_action_dec.

(** Specify the type of the result when [a] is executed *)
Definition action_res := da.action_res.
Definition eq_action_res_dec := da.eq_action_res_dec.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof.
  intros. destruct p1.
Defined.


(*************** [Domain] action marker *******************)

(** Marker expressing that the segment of messages [s] was generated during performing action [a] which produced the value [res]. *)
Axiom segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.
(* note the lack of [segment_of_action_with_res_dec]: it is not required to be able to retroactively identify segments after they are generated. *)
(* TODO investigate contains_segment_of_action_with_res to support parallel executions interleaved traces instead of forcing all domains to be serial Parameter contains_segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg)
    (trace : list w.msg),
    Prop. *)




(*************** [Domain] action and property interactions *******************)

(** Specify property propagations guaranteed by this domain.

If a trace [t] has property [p], executing action [a] which results in [res] and adds a segment of messages [s], does not interfere with property [p], that is trace [s ++ t] still has property [p].
*)
Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros. destruct p.
Defined.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros. destruct p.
Defined.

(** Specify backwards property propagations guaranteed by this domain.

If a trace [s++t] has property [p], where [s] is the segment of messages generated while executing action [a] which resulted in [res], then [p] already did hold on [t] as well.
*)
Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros. destruct p.
Defined.

Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p}.
Proof.
  intros. destruct p.
Defined.

(*************** [Domain] denotations *******************)

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

(** if a [prop] is closely linked to an [action] this helper creates trace property, where the action segment of [a] starts the valid range, and it propagates until [p] propagates *)
Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

(** Translate property ids into trace properties. *)
Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop.
Proof.
  intros. destruct p.
Defined.

Module ea := ExistentialAction w ior.
Definition plain_action :=
  ea.plain_action
    action_res
    segment_of_action_with_res
    (fun _ => True) (* all actions callable all the time *).

Axiom Word8 : Set.
Axiom Word82ascii : Word8 -> Ascii.ascii.
Axiom ascii2Word8 : Ascii.ascii -> Word8.
Axiom Word8asciiIsom1 : forall a, Word82ascii (ascii2Word8 a) = a.
Axiom Word8asciiIsom2 : forall w, ascii2Word8 (Word82ascii w) = w.

Axiom ByteString : Set.
Axiom ByteString2Word8list : ByteString -> list Word8.
Axiom Word8list2ByteString : list Word8 -> ByteString.
Axiom ByteStringWord8listIsom1 : forall bs, Word8list2ByteString (ByteString2Word8list bs) = bs.
Axiom ByteStringWord8listIsom2 : forall wl, ByteString2Word8list (Word8list2ByteString wl) = wl.

Definition ByteString2String bs :=
  (fix loop bs
   := match bs with
      | nil => String.EmptyString
      | (h::bs')%list => String.String (Word82ascii h) (loop bs')
      end) (ByteString2Word8list bs).
Definition String2ByteString s :=
  Word8list2ByteString
    ((fix loop s
      := match s with
         | String.EmptyString => nil
         | String.String h s' => ((ascii2Word8 h)::(loop s'))%list
         end) s).
Lemma StringByteStringIsomorphic :
  (forall s, ByteString2String (String2ByteString s) = s) /\
  (forall bs, String2ByteString (ByteString2String bs) = bs).
Proof.
  split.
  - intros. induction s.
    + unfold String2ByteString. unfold ByteString2String. rewrite ByteStringWord8listIsom2. reflexivity.
    + unfold String2ByteString in *. unfold ByteString2String in *. rewrite ByteStringWord8listIsom2 in *. rewrite IHs. rewrite Word8asciiIsom1. reflexivity.
  - intros. unfold String2ByteString. unfold ByteString2String.
    assert (Hx := ByteStringWord8listIsom1 bs).
    rewrite <- Hx at 2; clear Hx.
    apply f_equal.
    induction (ByteString2Word8list bs).
    + reflexivity.
    + rewrite IHl. rewrite Word8asciiIsom2. reflexivity.
Qed.

Axiom read' :
  forall
    (h : id.handle)
    (len : nat),
    ior.IO
      (fun _ => True)
      (result ByteString String.string)
      (fun r s t =>
         segment_of_action_with_res (fd_read h len) (ok_map ByteString2String r) s).
Definition read :
  forall
    (h : id.handle)
    (len : nat),
    plain_action
      (fd_read h len).
Proof.
  unfold plain_action. unfold ea.plain_action. intros.
  assert (m := read' h len).
  unfold action_res. unfold da.action_res.
  refine (io.IO_pure_map m (fun x => ok_map ByteString2String x) _ _).
  intros.
  exact H0.
Defined.

Axiom write' :
  forall
    (h : id.handle)
    (l : ByteString),
    ior.IO
      (fun _ => True)
      (result nat String.string)
      (fun r s t =>
         segment_of_action_with_res (fd_write h (ByteString2String l)) r s).
Definition write :
  forall
    (h : id.handle)
    (s : string),
    plain_action
      (fd_write h s).
Proof.
  intros.
  assert (m := write' h (String2ByteString s)).
  unfold plain_action. unfold ea.plain_action.
  rewrite (proj1 StringByteStringIsomorphic) in m. exact m.
Defined.

Axiom sync :
  forall
    (h : id.handle),
    plain_action
      (fd_sync h).

(** Translate an action id into a IO action *)
Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type }.
Proof.
  intros.
  magic_case a.
  - refine (existT _ (_, _) (read h n, _)); solve [ trivial ].
  - refine (existT _ (_, _) (write h s, _)); solve [ trivial ].
  - refine (existT _ (_, _) (sync h, _)); solve [ trivial ].
Defined.




(********************** [Domain] consistency *************************)
(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)
Lemma propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).
Proof.
  intros. destruct p.
Qed.

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)
Lemma back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.
Proof.
  intros. destruct p.
Qed.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

Module extractHaskell (x : DelayExtractionDefinitionToken).
Extract Constant Word8 => "Data.Word.Word8".
Extract Constant ascii2Word8 => "Prelude.head Prelude.. Data.ByteString.unpack Prelude.. Data.ByteString.Char8.pack Prelude.. Prelude.return".
Extract Constant Word82ascii => "Prelude.head Prelude.. Data.ByteString.Char8.unpack Prelude.. Data.ByteString.pack Prelude.. Prelude.return".
Extract Constant ByteString => "Data.ByteString.ByteString".
Extract Constant ByteString2Word8list => "Data.ByteString.unpack".
Extract Constant Word8list2ByteString => "Data.ByteString.pack".

Extract Constant read' => "\h n -> trymap (Data.ByteString.hGet h (Prelude.fromInteger n))".
Extract Constant write' => "\h s -> do el <- trymap (Data.ByteString.hPutNonBlocking h s); Prelude.return (Data.Bifunctor.second (\l -> Prelude.toInteger ((Data.ByteString.length s) Prelude.- (Data.ByteString.length l))) el)".
Extract Constant sync => "trymap Prelude.. System.IO.hFlush".
End extractHaskell.

Module extractOcaml (x : DelayExtractionDefinitionToken).
Extract Constant Word8 => "char".
Extract Constant ascii2Word8 => "(fun x -> x)".
Extract Constant Word82ascii => "(fun x -> x)".
Extract Constant ByteString => "string".
Extract Constant ByteString2Word8list => "(fun s -> let r = ref [] in String.iter (fun c -> r := c::!r) s; List.rev !r)".
Extract Constant Word8list2ByteString => "(fun l -> let r = Buffer.create (List.length l) in List.iter (Buffer.add_char r) l; Buffer.contents r)".

Extract Constant read' => "(fun h n -> trymap (fun () -> (let b = Bytes.create n in let r = Unix.read h b 0 n in String.sub (Bytes.unsafe_to_string b) 0 r)))".
Extract Constant write' => "(fun h s -> trymap (fun () -> (let wl = Unix.write h (Bytes.of_string s) 0 (String.length s) in wl)))".
Extract Constant sync => "(fun h -> trymap (fun () -> (failwith ""sync in Ocaml is not implemented"")))".
End extractOcaml.

End FDImpl.
