Require Import ElemHandle.

Require DelayExtractionDefinitionToken.

Module FDHandleImpl
<: ElemHandle
.

Axiom handle : Set.
Axiom eq_handle_dec :
  forall h1 h2 : handle,
    {h1=h2}+{h1<>h2}.

Module extractHaskell (x : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant handle => "System.IO.Handle".
Extract Constant eq_handle_dec => "(Prelude.==)".
End extractHaskell.

Module extractOcaml (x : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant handle => "Unix.file_descr".
Extract Constant eq_handle_dec => "(=)".
End extractOcaml.

End FDHandleImpl.
