Require Import String.

Require Import sublist.
Require Import ltac_utils.
Require Import cheap_app_assoc.
Require Import result.
Require Import dec_utils.

Require Import Beque.IO.intf.World.
Require Import Beque.IO.intf.IORaw.
Require Import Beque.IO.intf.IOAux.

Require Import Beque.LTL.util.LTL.

Require Import Beque.Frame.intf.array.ElemDomain.
Require Import Beque.Frame.intf.array.ElemHandle.
Require Import Beque.Frame.util.ExistentialAction.

Require Import FDActions.

Module Type FD
       (w : World)
       (ior : IORaw w)
       (id : ElemHandle)
<: ElemDomain w ior id
.

Inductive fd_prop :=.
(************************** first part of [Domain] ***************************)
Definition prop := fd_prop.

Module da := FDActions id.

Export da.

(************************** [ElemDomain] part ***************************)

Definition prop_for :
  prop -> id.handle.
Proof.
  intro. destruct H.
Defined.

Definition action_for
           a :=
  match a with
    | da.fd_read h _ => h
    | da.fd_write h _ => h
    | da.fd_sync h => h
  end.

(************************** rest of [Domain] ***************************)
Module io := IOAux w ior.
Module ltl := LTL w.


(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
Definition action := da.action.
Definition eq_action_dec := da.eq_action_dec.

(** Specify the type of the result when [a] is executed *)
Definition action_res := da.action_res.
Definition eq_action_res_dec := da.eq_action_res_dec.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof.
  intros. destruct p1.
Defined.


(*************** [Domain] action marker *******************)

(** Marker expressing that the segment of messages [s] was generated during performing action [a] which produced the value [res]. *)
Parameter segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.
(* note the lack of [segment_of_action_with_res_dec]: it is not required to be able to retroactively identify segments after they are generated. *)
(* TODO investigate contains_segment_of_action_with_res to support parallel executions interleaved traces instead of forcing all domains to be serial Parameter contains_segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg)
    (trace : list w.msg),
    Prop. *)




(*************** [Domain] action and property interactions *******************)

(** Specify property propagations guaranteed by this domain.

If a trace [t] has property [p], executing action [a] which results in [res] and adds a segment of messages [s], does not interfere with property [p], that is trace [s ++ t] still has property [p].
*)
Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros. destruct p.
Defined.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros. destruct p.
Defined.

(** Specify backwards property propagations guaranteed by this domain.

If a trace [s++t] has property [p], where [s] is the segment of messages generated while executing action [a] which resulted in [res], then [p] already did hold on [t] as well.
*)
Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros. destruct p.
Defined.

Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p}.
Proof.
  intros. destruct p.
Defined.

(*************** [Domain] denotations *******************)

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

(** if a [prop] is closely linked to an [action] this helper creates trace property, where the action segment of [a] starts the valid range, and it propagates until [p] propagates *)
Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

(** Translate property ids into trace properties. *)
Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop.
Proof.
  intros. destruct p.
Defined.

Module ea := ExistentialAction w ior.
Definition plain_action :=
  ea.plain_action
    action_res
    segment_of_action_with_res
    (fun _ => True) (* all actions callable all the time *).

Parameter read :
  forall
    (h : id.handle)
    (len : nat),
    plain_action
      (fd_read h len).

Parameter write :
  forall
    (h : id.handle)
    (s : string),
    plain_action
      (fd_write h s).

Parameter sync :
  forall
    (h : id.handle),
    plain_action
      (fd_sync h).

(** Translate an action id into a IO action *)
Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type }.
Proof.
  intros.
  magic_case a.
  - refine (existT _ (_, _) (read h n, _)); solve [ trivial ].
  - refine (existT _ (_, _) (write h s, _)); solve [ trivial ].
  - refine (existT _ (_, _) (sync h, _)); solve [ trivial ].
Defined.




(********************** [Domain] consistency *************************)
(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)
Lemma propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).
Proof.
  intros. destruct p.
Qed.

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)
Lemma back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.
Proof.
  intros. destruct p.
Qed.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

End FD.
