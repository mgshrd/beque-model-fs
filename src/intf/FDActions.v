Require Import String.

Require Import dec_utils.
Require Import result.

Require Import Beque.Frame.intf.array.ElemHandle.

Module FDActions
       (id : ElemHandle)
.

Inductive fd_action :=
| fd_read : id.handle -> nat -> fd_action
| fd_write : id.handle -> string -> fd_action
| fd_sync : id.handle -> fd_action.
Definition action := fd_action.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  assert (ne := PeanoNat_Nat_eq_dec).
  assert (he := id.eq_handle_dec).
  assert (le := string_dec).
  decide equality.
Defined.

Definition action_res
           (a : action) :
  Set :=
  match a with
    | fd_read _ _ => result string string
    | fd_write _ _ => result nat string
    | fd_sync _ => result unit string
  end.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  assert (ne := PeanoNat_Nat_eq_dec).
  assert (he := id.eq_handle_dec).
  assert (le := string_dec).
  assert (ue := eq_unit_dec).
  destruct a; simpl in *; decide equality.
Defined.

End FDActions.
