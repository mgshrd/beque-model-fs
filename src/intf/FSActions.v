Require Import result.
Require Import dec_utils.

Require Import Beque.IO.intf.World.
Require Import Beque.IO.intf.IORaw.

Require Import Beque.Frame.intf.array.ElemHandle.
Require Import Beque.Frame.intf.array.ElemDomain.
Require Import Beque.Frame.intf.array.DomainArrayActions.

Require Import FD.

(** * A collection of actions split out from FS to support Embedding. *)
Module Type FSActions
       (w : World)
       (ior : IORaw w)
       (subid : ElemHandle)
       (subd : FD w ior subid)
<: DomainArrayActions w ior subid subd
.

(** The name identifying a subdomain instance. *)
Parameter name :
  Set.
Parameter eq_name_dec :
  forall n1 n2 : name, {n1=n2}+{n1<>n2}.

(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
Inductive dw_action :
  Set :=
| dw_action_open : name -> dw_action
| dw_action_close : subid.handle -> dw_action
| dw_action_operate : subd.action -> dw_action.
Definition action := dw_action.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  assert (ne := eq_name_dec).
  assert (he := subid.eq_handle_dec).
  assert (ae := subd.eq_action_dec).
  decide equality.
Defined.

(** The type of errors produced while operating in the array domain *)
Definition error_code := String.string.
Definition eq_error_code_dec := String.string_dec.

(** Specify the type of the result when [a] is executed *)
Definition action_res
           (a : action) :
  Set :=
  match a with
    | dw_action_open _ => result subid.handle error_code
    | dw_action_close _ => result unit error_code
    | dw_action_operate sa => subd.action_res sa
  end.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  assert (ne := eq_name_dec).
  assert (he := subid.eq_handle_dec).
  assert (ee := eq_error_code_dec).
  assert (ae := subd.eq_action_dec).
  assert (are := subd.eq_action_res_dec).
  assert (ue := eq_unit_dec).
  destruct a; intros; try decide equality.
  apply are.
Defined.

End FSActions.
