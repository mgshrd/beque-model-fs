Require Import List.
Require Import Omega.

Require Import sublist.
Require Import ltac_utils.
Require Import ltac_seg.
Require Import ltac_sublist.

Require Import sublist_facts.
Require Import list_eq_part.
Require Import case_tag.
Require Import DelayExtractionDefinitionToken.

Require Import Beque.IO.intf.World.
Require Import Beque.IO.intf.IORaw.
Require Import Beque.IO.intf.IOAux.

Require Import Beque.LTL.util.LTL.
Require Import Beque.LTL.util.LTLUtilFacts.

Require Import Beque.Frame.intf.array.DomainArray.
Require Import Beque.Frame.intf.array.DomainArrayActions.
Require Import Beque.Frame.intf.array.ElemHandle.

Require Import FD.
Require Import FSActions.

(** * A collection of actions and properties for a filesystem. *)
Module Type FS
       (w : World)
       (ior : IORaw w)

       (subid : ElemHandle)
       (subd : FD w ior subid)

       (daa : FSActions w ior subid subd)
<: DomainArray w ior subid subd daa
.

Module io := IOAux w ior.
Module ltl := LTL w.
Module ltluf := LTLUtilFacts w.

Export daa.

(*************** Basic [Domain] data *******************)
(** The properties this domain defines. *)
Inductive dw_prop :
  Set :=
| dw_prop_isopenfor : subid.handle -> name -> dw_prop
| dw_prop_subprop : subd.prop -> dw_prop.
Definition prop := dw_prop.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof.
  assert (ne := daa.eq_name_dec).
  assert (he := subid.eq_handle_dec).
  assert (pe := subd.eq_prop_dec).
  Set Keep Proof Equalities.
  decide equality.
Defined.


(*************** [Domain] action marker *******************)

(** Marker expressing that the segment of messages [s] was generated during performing action [a] which produced the value [res]. *)
Parameter segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.
(* note the lack of [segment_of_action_with_res_dec]: it is not required to be able to retroactively identify segments after they are generated. *)
(* TODO investigate contains_segment_of_action_with_res to support parallel executions interleaved traces instead of forcing all domains to be serial Parameter contains_segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg)
    (trace : list w.msg),
    Prop. *)




(*************** [Domain] action and property interactions *******************)

(** Implementations must state, whether action [a] on handle [h] with result [res] is idempotent regarding property [p] of a handle different than [h] (cf. [crosspf]) *)
Parameter other_domain_action_idempotent :
  forall
    (a : subd.action)
    (res : subd.action_res a)
    (p : subd.prop)
    (crosspf : subd.action_for a <> subd.prop_for p),
    Prop.
Parameter other_domain_action_idempotent_dec :
  forall a res p pf,
    { other_domain_action_idempotent a res p pf}+
    {~other_domain_action_idempotent a res p pf}.

(** Specify property propagations guaranteed by this domain.

If a trace [t] has property [p], executing action [a] (which will add a segment of messages [s]), does not interfere with property [p], that is trace [s ++ t] still has property [p]. C.f. [propagate_segment]
*)
Definition propagates
           (a : action)
           (res : action_res a)
           (p : prop) :
  Prop :=
  match a as ax, res, p with
    | dw_action_open _, _, dw_prop_isopenfor _ _ =>
        (** opening a new domain does not close the olds *)
        True
    | dw_action_open _, _, dw_prop_subprop _ =>
        (** subdomain properties don't change because of a new one *)
        True
    | dw_action_close h, _, dw_prop_isopenfor h' _ =>
        (** closing only affects the opens status for the closed handle, regardless of the result *)
        h <> h'
    | dw_action_close h, _, dw_prop_subprop sp =>
        (** closing a different domain propagates other subdomain's properties *)
        h <> subd.prop_for sp
    | dw_action_operate _, _, dw_prop_isopenfor _ _ =>
        (** subdomain operations don't affect reference validity *)
        True
    | dw_action_operate op, subdres, dw_prop_subprop sp =>
        if subid.eq_handle_dec (subd.action_for op) (subd.prop_for sp)
        then subd.propagates op subdres sp (** subdomain property propagation is forwarded *)
        else other_domain_action_idempotent op subdres sp ltac:(assumption)
  end.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a.
  - compute. left. destruct p; exact I.
  - compute. destruct p.
    + destruct (subid.eq_handle_dec h h0).
      * right. intros. elim H. exact e.
      * left. intro. elim n0. exact H.
    + destruct (subid.eq_handle_dec h (subd.prop_for p)).
      * subst h. right. intro. elim H. reflexivity.
      * left. assumption.
  - destruct p; [ left; compute; exact I | ].
    simpl. destruct (subid.eq_handle_dec _ _).
    + match goal with
          |- {_ ?a ?x ?p}+{_} =>
          destruct (subd.propagates_dec a x p); [ left | right ]
      end; assumption.
    + exact (other_domain_action_idempotent_dec _ _ _ _).
Defined.

Definition back_propagates
           (a : action)
           (res : action_res a)
           (p : prop) :
  Prop :=
  match a as ax, res, p with
    | dw_action_open _, r, dw_prop_isopenfor h' _ =>
        (** opening a domain only affects its own open status *)
        r <> result.ok h'
    | dw_action_open _, r, dw_prop_subprop sp =>
        (** subdomain properties are invalidated if their domain is unopened *)
        r <> result.ok (subd.prop_for sp)
    | dw_action_close _, _, dw_prop_isopenfor _ _ =>
        (** closing only affects the opens status for the closed handle *)
        True
    | dw_action_close h, _, dw_prop_subprop _ =>
        (** closing cannot add to subdomain properties *)
        True
    | dw_action_operate op, _, dw_prop_isopenfor _ _ =>
        (** subdomain operations don't affect reference validity *)
        True
    | dw_action_operate op, subdres, dw_prop_subprop sp =>
        if subid.eq_handle_dec (subd.action_for op) (subd.prop_for sp)
        then subd.back_propagates op subdres sp (** subdomain property back-propagation is forwarded *)
        else other_domain_action_idempotent op subdres sp ltac:(assumption)
  end.

Definition back_propagates_dec
           (a : action)
           (res : action_res a)
           (p : prop) :
  {back_propagates a res p}+{~back_propagates a res p}.
Proof.
  mcase a; simpl.
  - mcase p.
    + destruct (eq_action_res_dec _ res (result.ok h)), (eq_name_dec n n0); tauto.
    + destruct (eq_action_res_dec _ res (result.ok (subd.prop_for p))); tauto.
  - left; destruct p; exact I.
  - mcase p; [ left; exact I | ].
    destruct subid.eq_handle_dec.
    + apply subd.back_propagates_dec.
    + apply other_domain_action_idempotent_dec.
Defined.

(*************** [Domain] denotations *******************)

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

(** if a [prop] is closely linked to an [action] this helper creates trace property, where the action segment of [a] starts the valid range, and it propagates until [p] propagates *)
Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

(** Translate property ids into trace properties. *)
Definition prop_to_trace_prop
           (p : prop) :
  list w.msg -> Prop :=
  match p with
    | dw_prop_isopenfor h n =>
      simple_propagation (dw_action_open n) (result.ok h) (dw_prop_isopenfor h n)
    | dw_prop_subprop sp =>
      subd.prop_to_trace_prop sp
  end.

Definition isopenfor h n := prop_to_trace_prop (dw_prop_isopenfor h n).
Definition isvalidref h := fun t => exists n, isopenfor h n t.

Parameter open :
  forall (n : name),
    ior.IO
      (fun t => forall h, ~isopenfor h n t (* only exclusive access is allowed for a domain *))
      (action_res (dw_action_open n))
      (fun res s t =>
         segment_of_action_with_res (dw_action_open n) res s /\
         match res with
         | result.ok h =>
           isopenfor h n (s ++ t) /\
           (forall h', isvalidref h' t -> h <> h') (* each valid ref must be unique *)
         | result.err _ =>
           True
         end).

Parameter close :
  forall (h : subid.handle),
    ior.IO
      (isvalidref h)
      (action_res (dw_action_close h))
      (fun res s t =>
         segment_of_action_with_res (dw_action_close h) res s).

Parameter operate_on :
  forall (op : subd.action),
    ior.IO
      (isvalidref (subd.action_for op))
      (action_res (dw_action_operate op))
      (fun res s t =>
         segment_of_action_with_res (dw_action_operate op) res s).

Definition read h len := operate_on (subd.da.fd_read h len).
Definition write h l := operate_on (subd.da.fd_write h l).
Definition sync h := operate_on (subd.da.fd_sync h).

(** Translate an action id into a IO action *)
Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type }.
Proof.
  intros. magic_case a.
  - refine (let Hx := io.IO_weaken
                        (open n)
                        (fun res s t => segment_of_action_with_res (dw_action_open n) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. destruct H1. exact H1.
  - refine (let Hx := io.IO_weaken
                        (close h)
                        (fun res s t => segment_of_action_with_res (dw_action_close h) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. exact H1.
  - refine (let Hx := io.IO_weaken
                        (operate_on a0)
                        (fun res s t => segment_of_action_with_res (dw_action_operate a0) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. exact H1.
Defined.

Conjecture segment_inversion__action :
  forall {a a' res res' s},
    segment_of_action_with_res a res s ->
    segment_of_action_with_res a' res' s ->
    a = a'.

Conjecture segment_inversion__result :
  forall {a res res' s},
    segment_of_action_with_res a res s ->
    segment_of_action_with_res a res' s ->
    res = res'.

Parameter segment_entirety :
  forall {a res s},
    segment_of_action_with_res a res s ->
    forall s',
      sublist_of s' s ->
      s' <> s ->
      forall {a' res'},
        ~segment_of_action_with_res a' res' s'.

Corollary segment_inversion__segment :
  forall {a a' res res' s s'},
    segment_of_action_with_res a res s ->
    segment_of_action_with_res a' res' s' ->
    sublist_of s' s ->
    a = a' /\ s = s'.
Proof.
  intros. destruct H1. destruct H1. subst s.
  destruct x; [ destruct x0 | ].
  - rewrite List.app_nil_r in *. split; [ | reflexivity ].
    assert (Hx := segment_inversion__action H H0).
    assumption.
  - exfalso. assert (Hx := fun pf pf2 => segment_entirety H s' pf pf2 H0). elim Hx.
    + exists nil. exists (m::x0). reflexivity.
    + simpl. intro. lengthelim H1. omega.
  - exfalso. assert (Hx := fun pf pf2 => segment_entirety H s' pf pf2 H0). elim Hx.
    + exists (m::x). exists x0. reflexivity.
    + intro. lengthelim H1. omega.
Qed.

Conjecture segment_no_recombine :
  forall {a r s},
    segment_of_action_with_res a r s ->
    forall endofs,
      proper_postfix_of endofs s ->
      forall {a' r' s'},
        prefix_of endofs s' ->
        ~segment_of_action_with_res a' r' s'.

Conjecture segment_not_nil :
  forall {a res},
    ~segment_of_action_with_res a res nil.

Conjecture subd_propagates_correct :
  forall {sp sa res s t},
    let p := dw_prop_subprop sp in
    let a := dw_action_operate sa in
    segment_of_action_with_res a res s ->
    propagates a res p ->
    prop_to_trace_prop p t ->
    prop_to_trace_prop p (s ++ t).

Conjecture subd_back_propagates_correct :
  forall sp sa res s t,
    let p := dw_prop_subprop sp in
    let a := dw_action_operate sa in
    prop_to_trace_prop p (s ++ t) ->
    segment_of_action_with_res a res s ->
    back_propagates a res p ->
    prop_to_trace_prop p t.

(* this is expected to be an axiom in the implementations *)
Axiom open_propagates :
  forall {n res s},
    segment_of_action_with_res (dw_action_open n) res s ->
    forall {p t},
      subd.prop_to_trace_prop p t ->
      subd.prop_to_trace_prop p (s ++ t).

Axiom open_back_propagates :
  forall {n r s},
    segment_of_action_with_res (dw_action_open n) r s ->
    forall {p},
      r <> result.ok (subd.prop_for p) ->
      forall {t},
        subd.prop_to_trace_prop p (s ++ t) ->
        subd.prop_to_trace_prop p t.

(* this is expected to be an axiom in the implementations *)
Axiom close_propagates :
  forall {h res s},
    segment_of_action_with_res (dw_action_close h) res s ->
    forall {p},
      h <> subd.prop_for p ->
      forall {t},
        subd.prop_to_trace_prop p t ->
        subd.prop_to_trace_prop p (s ++ t).

Axiom close_back_propagates :
  forall {h res s},
    segment_of_action_with_res (dw_action_close h) res s ->
    forall {p t},
      subd.prop_to_trace_prop p (s ++ t) ->
      subd.prop_to_trace_prop p t.

Ltac sameseg H H0 :=
  let Hx := fresh in
  let Hy := fresh in
  assert (Hx := segment_inversion__action H0 H);
  match type of Hx with
  | ?x = _ => subst x
  | _ = ?x => subst x
  end;
  assert (Hy := segment_inversion__result H0 H);
  match type of Hy with
  | ?x = _ => subst x
  | _ = ?x => subst x
  end.

Lemma simple_propagation_apply :
  forall {a' res' a res p s t},
    segment_of_action_with_res a' res' s ->
    propagates a' res' p ->
    simple_propagation a res p t ->
    simple_propagation a res p (s ++ t).
Proof.
  intros. unfold simple_propagation, ltl.tillnow in *.
  sega. exists (s ++ x). exists x0.
  split; [ rewrite <- List.app_assoc; reflexivity | ].
  split; [ exact H1 | ].
  intros. destruct H3. pfelim H3; [ | apply H2 | apply H2 ].
  - subst s. subst s'. clear H3.
    destruct x1; [ right | left ].
    + exists a'. exists res'. exists x2.
      split; [ | split; [ exact H | exact H0 ] ].
      exists (x ++ x0). rewrite <- List.app_assoc. reflexivity.
    + unfold starts_with_partial_segment. intros. intro.
      destruct H3. rewrite <- List.app_assoc in H3. pfelim H3.
      * subst x2. subst x3. clear H3.
        assert (Hx := segment_inversion__segment H H5). destruct Hx.
        { exists (m::x1). exists x4. reflexivity. }
        lengthelim (eq_sym H7). omega.
      * subst x2. subst x3.
        assert (Hx := segment_inversion__segment H H5). destruct Hx.
        { exists (m::x1). exists nil. rewrite List.app_nil_r. reflexivity. }
        lengthelim (eq_sym H7). omega.
      * subst s'. clear H3.
        assert (Hx := fun pf pf2 => segment_no_recombine H x2 pf pf2 H5).
        elim Hx; [ | exists x4; reflexivity ].
        exists (m::x1).
        split; [ assumption | ].
        split; [ | reflexivity ].
        intro. lengthelim H3. omega.

  - exists nil. assumption.

  - exists x2. assumption.
Qed.

Lemma simple_propagation_back_apply :
  forall {a' res' a res p s t},
    segment_of_action_with_res a' res' s ->
    simple_propagation a res p (s ++ t) ->
    (segment_of_action_with_res a res s \/
     (propagates a' res' p /\ simple_propagation a res p t)).
Proof.
  intros. unfold simple_propagation in H0.

  assert (Hx := ltluf.tillnow_unfold_seg _ _ _ H0); clear H0.
  destruct Hx. destruct H0. destruct H0. destruct H1.
  pfelim H0.

  - subst s. subst x0.
    clear H0.
    destruct H1. destruct H0. destruct H0. pfelim H0.
    + subst x1. subst x2.
      assert (Hx := fun pf pf2 => segment_entirety H _ pf pf2 H1). elim Hx.
      * exists x. exists x3. reflexivity.
      * intro. destruct x3; [ elim H4; reflexivity | ].
        lengthelim H5. omega.
    + subst x1. subst t. assert (Hx := segment_inversion__segment H H1). destruct Hx.
      { exists x. exists nil. rewrite List.app_nil_r. reflexivity. }
      subst a'. destruct x; [ | lengthelim (eq_sym H5); omega ].
      left. exact H1.
    + subst x0. subst t.
      clear H0.
      destruct x;
        [ assert (Hx := fun pf pf2 => segment_entirety H1 _ pf pf2 H)
        | assert (Hx := fun pf pf2 => segment_no_recombine H x1 pf pf2 H1)
        ].
      * elim Hx.
        { exists nil. exists x3. reflexivity. }
        { intro. destruct x3; [ elim H4; reflexivity | ]. lengthelim H0. omega. }
      * elim Hx.
        {
          exists (m::x).
          split; [ assumption | ].
          split; [ | reflexivity ].
          intro. lengthelim H0. omega.
        }
        {
          exists x3. reflexivity.
        }

  - subst x. subst x0. clear H0.
    right. split.
    + assert (H2' := H2 s (ex_intro _ nil eq_refl)); clear H2. destruct H2'.
      * elim (H0 _ _ _ (ex_intro _ t eq_refl) H).
      * destruct H0. destruct H0. destruct H0. destruct H0. destruct H2.
        destruct H0. pfelim H0.
        {
          subst s. subst x2. assert (Hx := fun pf pf2 => segment_entirety H _ pf pf2 H2). elim Hx.
          - exists nil. exists x3. reflexivity.
          - intro. destruct x3; [ elim H4; reflexivity | ]. lengthelim H5. omega.
        }
        {
          subst x1. subst x2. sameseg H H2. exact H3.
        }
        {
          subst x1. subst t. clear H0.
          assert (Hx := fun pf pf2 => segment_entirety H2 _ pf pf2 H). elim Hx.
          - exists nil. exists x3. reflexivity.
          - intro. destruct x3; [ elim H4; reflexivity | ]. lengthelim H0. omega.
        }
    + unfold simple_propagation. unfold ltl.tillnow.
      exists nil. exists t.
      split; [ reflexivity | ].
      split; [ exact H1 | ].
      intros. destruct H0. destruct x; [ destruct s'; [ | discriminate H0 ] | discriminate H0 ].
      clear H0. simpl. assert (H2' := H2 nil); clear H2.
      simpl in H2'. apply H2'.
      exists s. rewrite List.app_nil_r. reflexivity.

  - subst x. subst t. clear H0. rename x0 into t. rename x1 into s'. rename s into s''.
    right; split. all: cycle 1.
    + unfold simple_propagation, ltl.tillnow.
      exists s'. exists t.
      split; [ reflexivity | ].
      split; [ assumption | ].
      intros. apply H2.
      destruct H0. subst s'. exists (s'' ++ x). rewrite <- List.app_assoc. reflexivity.
    + assert (postfix_of (s'' ++ s') (s'' ++ s')).
      { exists nil. reflexivity. }
      destruct (H2 _ H0); clear H2.
      * elim (fun pf => H4 _ _ _ pf H).
        exists (s' ++ t). rewrite <- List.app_assoc. reflexivity.
      * destruct H4. destruct H2. destruct H2. explode H2.
        destruct H2. rewrite <- List.app_assoc in H2. pfelim H2.
        subst s''. subst x2. clear H2.
        {
          assert (Hx := fun pf pf2 => segment_entirety H _ pf pf2 H4). elim Hx.
          - exists nil. exists x3. reflexivity.
          - intro. lengthelim H2. destruct x3; [ elim H6; reflexivity | ]. simpl in H7. omega.
        }
        {
          subst x1. subst x2. sameseg H H4. assumption.
        }
        {
          subst x1. assert (Hx := fun pf pf2 => segment_entirety H4 _ pf pf2 H). elim Hx.
          - exists nil. exists x3. reflexivity.
          - intro. lengthelim H7. destruct x3; [ elim H6; reflexivity | ]. simpl in H9. omega.
        }
Qed.

(********************** [Domain] consistency *************************)
(** consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)
Lemma propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).
Proof.
  intros. mcase p.

  - exact (simple_propagation_apply H0 H H1).

  - mcase a.
    + exact (open_propagates H0 H1).
    + exact (close_propagates H0 H H1).
    + exact (subd_propagates_correct H0 H H1).
Qed.

(** consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)
Lemma back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.
Proof.
  intros. mcase p.

  - unfold prop_to_trace_prop in *.
    mcase (simple_propagation_back_apply H0 H1).
    + sameseg H0 s0; clear s0.
      simpl in *. elim H. reflexivity.
    + destruct a0. assumption.

  - unfold prop_to_trace_prop in *. unfold back_propagates in H. mcase a.
    + assert (Hx := open_back_propagates H0 H H1). exact Hx.
    + assert (Hx := close_back_propagates H0 H1). exact Hx.
    + assert (Hx := subd_back_propagates_correct p a res s t).
      unfold back_propagates in Hx.
      destruct (subid.eq_handle_dec); apply Hx; auto.
Qed.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

(* Forward daa definitions *)
Definition name := daa.name.
Definition eq_name_dec := daa.name.
Definition action := daa.action.
Definition eq_action_dec := daa.eq_action_dec.

Definition action_res := daa.action_res.
Definition eq_action_res_dec := daa.eq_action_res_dec.

Module extractHaskell (token : DelayExtractionDefinitionToken).
End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken).
End extractOcaml.

End FS.
